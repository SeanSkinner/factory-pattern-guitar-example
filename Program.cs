﻿using System;
using System.Collections.Generic;

namespace PickupWorkshop
{
    class Program
    {
        static void Main(string[] args)
        {
            var standardStratConfigurationFactory = new StandardFenderStratPickupFactory();
            var metalGuitarConfigurationFactory = new MetalGuitarConfigurationFactory();

            var fenderGuitar = new ElectricGuitar { Configuration = standardStratConfigurationFactory.CreateConfiguration() };
            var metalGuitar = new ElectricGuitar { Configuration = metalGuitarConfigurationFactory.CreateConfiguration() };
            var superFender = new ElectricGuitar { Configuration = metalGuitarConfigurationFactory.CreateConfiguration() };

            List<ElectricGuitar> guitars = new List<ElectricGuitar>();

            guitars.Add(fenderGuitar);
            guitars.Add(metalGuitar);
            guitars.Add(superFender);

            foreach (var guitar in guitars)
            {
                Console.WriteLine(guitar.Configuration.ToString());
            }

            WorkshopSingleton.GetWorkshopSingleton().ConstructGuitar(metalGuitar);
        }
    }
}
