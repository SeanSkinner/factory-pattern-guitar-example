﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PickupWorkshop
{
    //Not thread-safe, just a simple example to illustrate the overall pattern, use locking for thread sensitive applications
    public class WorkshopSingleton
    {
        private static WorkshopSingleton instance;
        private List<string> sections = new List<string>();
        private Random rand = new Random();

        private WorkshopSingleton()
        {
            sections.Add("A1");
            sections.Add("A2");
            sections.Add("A3");
            sections.Add("B1");
            sections.Add("B2");
            sections.Add("B3");
            sections.Add("C1");
            sections.Add("C2");
            sections.Add("C3");
        }

        public static WorkshopSingleton GetWorkshopSingleton()
        {
            if (instance == null)
            {
                instance = new WorkshopSingleton();
            }

            return instance;
        }

        public void ConstructGuitar(ElectricGuitar guitar)
        {
            Console.WriteLine($"{guitar.Configuration} is being assembled on section {instance.sections[rand.Next(0, sections.Count)]}");
        }
    }
}
