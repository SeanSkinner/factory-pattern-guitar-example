﻿namespace PickupWorkshop
{
    public enum PickupSlot
    {
        Neck,
        Middle,
        Bridge
    }
}