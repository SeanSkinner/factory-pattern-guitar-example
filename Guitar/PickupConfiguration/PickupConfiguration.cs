﻿using System.Collections.Generic;

namespace PickupWorkshop
{
    public abstract class PickupConfiguration
    {
        //PickupSlot, Pickup
        public Dictionary<PickupSlot, Pickup> Configuration { get; set; } = new Dictionary<PickupSlot, Pickup>();

        public override string ToString() 
        {
            string pickups = "";

            foreach (var pickup in Configuration)
            {
                pickups += $"{pickup.Key}: {pickup.Value.Model}\n";
            }

            return pickups;
        }
    }
}