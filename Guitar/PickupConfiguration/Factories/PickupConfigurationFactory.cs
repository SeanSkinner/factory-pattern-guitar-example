﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PickupWorkshop
{
    public abstract class PickupConfigurationFactory
    {
        public abstract PickupConfiguration CreateConfiguration();
    }
}
