﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PickupWorkshop
{
    public class MetalGuitarConfigurationFactory : PickupConfigurationFactory
    {
        public override PickupConfiguration CreateConfiguration()
        {
            var configuration = new MetalGuitarConfiguration();

            configuration.Configuration.Add(PickupSlot.Neck, new HumbuckerPickup { Model = "EMG 85" });
            configuration.Configuration.Add(PickupSlot.Bridge, new HumbuckerPickup { Model = "EMG 81" });

            return configuration;
        }
    }
}
