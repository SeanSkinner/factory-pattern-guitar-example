﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PickupWorkshop
{
    public class StandardFenderStratPickupFactory : PickupConfigurationFactory
    {
        public override PickupConfiguration CreateConfiguration()
        {
            var configuration = new StandardFenderStratConfiguration();

            configuration.Configuration.Add(PickupSlot.Neck, new SingleCoilPickup { Model = "Fender Noiseless Pickup" });
            configuration.Configuration.Add(PickupSlot.Middle, new SingleCoilPickup { Model = "Fender Noiseless Pickup" });
            configuration.Configuration.Add(PickupSlot.Bridge, new SingleCoilPickup { Model = "Fender Noiseless Pickup" });

            return configuration;
        }
    }
}
